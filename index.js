var express = require('express');
var app = express();
var router = require('./routes/')

app.use(router)
app.listen(process.env.PORT || 8080, function () {
  console.log('Listening');
})